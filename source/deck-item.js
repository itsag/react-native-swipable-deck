// Modules
import React from "react";
import { View } from "react-native";
import PropTypes from "prop-types";

// Component
const DeckItem = ({ style, children }) => {
  return <View style={style}>{children}</View>;
};

// Prop Types
DeckItem.propTypes = {
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array,
  ]),
  onSwipedLeft: PropTypes.func,
  onSwipedRight: PropTypes.func,
  onSwipedTop: PropTypes.func,
  onSwipedBottom: PropTypes.func,
  onSwiped: PropTypes.func,
};

// Default Props
DeckItem.defaultProps = {
  style: {},
  onSwiped: () => {},
  onSwipedLeft: () => {},
  onSwipedRight: () => {},
  onSwipedTop: () => {},
  onSwipedBottom: () => {},
};

// Exports
export default DeckItem;
