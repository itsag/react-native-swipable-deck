# Swipable Deck for React Native

React Native component for a deck with swipable items

---

```
Attention!

This library is built for internal use at Siac. There may be frequent changes in the functionality or new versions will introduce breaking changes without any prior notice.
```

---

## Installation

```
yarn add @siac/react-native-swipable-deck
```

---

## Usage

```javascript
import { Deck, DeckItem } from "@siac/react-native-swipable-deck";
```

```javascript
<Deck
  style={styles.content}
  ref={(stack) => {
    this.stack = stack;
  }}
>
  <DeckItem style={[styles.card, styles.card1]}>
    <Text style={styles.label}>A</Text>
  </DeckItem>
  <DeckItem style={[styles.card, styles.card2]}>
    <Text style={styles.label}>B</Text>
  </DeckItem>
  <DeckItem style={[styles.card, styles.card1]}>
    <Text style={styles.label}>C</Text>
  </DeckItem>
</Deck>
```

---

## Components

### Deck

#### Deck props

| Props               | type   | description                      | required | default                     |
| ------------------- | ------ | -------------------------------- | -------- | --------------------------- |
| style               | object | container style                  |          | {}                          |
| cardContainerStyle  | object | cardContainerStyle style         |          | {}                          |
| secondCardZoom      | number | second card zoom                 |          | 0.95                        |
| duration            | number | animation duration               |          | 300                         |
| initialIndex        | number | initial card index               |          | 0                           |
| loop                | bool   | keep swiping indefinitely        |          | false                       |
| renderNoMoreCards   | func   |                                  |          | false                       |
| disableTopSwipe     | bool   | disable top swipe                |          | false                       |
| disableBottomSwipe  | bool   | disable bottom swipe             |          | false                       |
| disableLeftSwipe    | bool   | disable left swipe               |          | false                       |
| disableRightSwipe   | bool   | disable right swipe              |          | false                       |
| verticalSwipe       | bool   | enable/disable vertical swiping  |          | true                        |
| horizontalSwipe     | bool   | enable/disable horizont swiping  |          | true                        |
| verticalThreshold   | number | vertical swipe threshold         |          | height/4                    |
| horizontalThreshold | number | horizontal swipe threshold       |          | width/2                     |
| outputRotationRange | array  | rotation values for the x values |          | ['-15deg', '0deg', '15deg'] |

#### Deck events

| Props          | type       | description                                                                               |
| -------------- | ---------- | ----------------------------------------------------------------------------------------- |
| onSwipeStart   | func       | function to be called when a card swipe starts                                            |
| onSwipeEnd     | func       | function to be called when a card swipe ends (card is released)                           |
| onSwiped       | func       | function to be called when a card is swiped. it receives the swiped card index            |
| onSwipedLeft   | func       | function to be called when a card is swiped left. it receives the swiped card index       |
| onSwipedRight  | func       | function to be called when a card is swiped right. it receives the swiped card index      |
| onSwipedTop    | func       | function to be called when a card is swiped top. it receives the swiped card index        |
| onSwipedBottom | func       | function to be called when a card is swiped bottom. it receives the swiped card index     |
| onSwipedAll    | async func | function to be called when the last card is swiped. Could trig action to refresh cards    |
| onSwipe        | func       | function to be called when a card is swiped. It receives the current x, and y coordinates |

#### Deck actions

| Props            | type |
| ---------------- | ---- |
| swipeLeft        | func |
| swipeRight       | func |
| swipeBottom      | func |
| swipeTop         | func |
| goBackFromLeft   | func |
| goBackFromRight  | func |
| goBackFromBottom | func |
| goBackFromTop    | func |

```javascript
  <Deck style={styles.content} ref={swiper => { this.swiper = swiper }}>
    <DeckItem style={[styles.card, styles.card1]}><Text style={styles.label}>A</Text></DeckItem>
    <DeckItem style={[styles.card, styles.card2]}><Text style={styles.label}>B</Text></DeckItem>
  </Deck>

  <TouchableOpacity onPress={ () => { this.swiper.swipeLeft() }}>
    <Text>Left</Text>
  </TouchableOpacity>
```

### DeckItem

#### DeckItem props

| Props | type   | description     | required | default |
| ----- | ------ | --------------- | -------- | ------- |
| style | object | container style |          | {}      |

#### DeckItem events

| Props          | type | description                                         |
| -------------- | ---- | --------------------------------------------------- |
| onSwiped       | func | function to be called when a card is swiped.        |
| onSwipedLeft   | func | function to be called when a card is swiped left.   |
| onSwipedRight  | func | function to be called when a card is swiped right.  |
| onSwipedTop    | func | function to be called when a card is swiped top.    |
| onSwipedBottom | func | function to be called when a card is swiped bottom. |

---

## License

`Swipable Deck is licensed under the MIT license.`
